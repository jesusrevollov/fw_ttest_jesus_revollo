# fw_ttest_jesus_revollo 

fw_ttest_jesus_revollo is an ESP-IDF project that implements communication with an Azure IoT Hub.


## Installation
This proccess considers that you have ESP-IDF installed and set-up correctly.
1. Clone ESP Azure IoT SDK repository from GitHub (https://github.com/espressif/esp-azure)
2. Clone this repository to the examples path of the Azure IoT repository (esp-azure\examples)

## Usage

Set the WiFi parameters (main/azure_main.c)

```c
#define EXAMPLE_WIFI_SSID "YourSSID"
#define EXAMPLE_WIFI_PASS "YourWiFiPass"
```
Set the AZ IoT Hub connection string (main/iothub_client_sample_mqtt.c)
```c
#define #define EXAMPLE_IOTHUB_CONNECTION_STRING "HostName=<host_name>;DeviceId=<device_id>;SharedAccessKey=<device_key>"
```
Finally build the project!
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
TBD